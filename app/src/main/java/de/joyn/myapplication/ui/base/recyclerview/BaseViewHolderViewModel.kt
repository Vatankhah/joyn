package de.joyn.myapplication.ui.base.recyclerview

open class BaseViewHolderViewModel<Model> {

    var `object`: Model? = null
}
