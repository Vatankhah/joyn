package de.joyn.myapplication.ui.base

abstract class BaseViewState {

    companion object {
        val STATUS_SUCCESS = 1
        val STATUS_FAILED = 2
    }
}